import flask
from Model.prediction import server_loaded_model
import numpy as np
from flask import Flask, request, send_file
app = Flask(__name__)


model = server_loaded_model("./Model/model_hourly_new_architecture_trainall_new.json", "./Model/model_hourly_new_architecture_trainall_new.h5", "./Model/standardizer_hourly.p")


@app.route('/')
def hello():
    return flask.render_template('index.html')

@app.route('/predict', methods=['POST'])
def predict():
    form_data = [x for x in request.form.values()]
    features = [float(x) for x in form_data[1:]]
    fname = form_data[0]
    model.perform_prediction_into_csv(fname, np.array(features))
    path = 'test.csv'
    return send_file(path, as_attachment=True)



if __name__ == '__main__':
    app.run(host='0.0.0.0')
    #app.run(host="0.0.0.0", ssl_context=('/home/user/building_energy/SSL/fullchain1.pem', '/home/user/building_energy/SSL/privkey1.pem'))
