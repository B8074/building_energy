FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y gcc python3 python3-dev python3-pip
RUN python3 -m pip install --upgrade pip

RUN pip3 install flask tensorflow h5py==2.10.0 pandas sklearn

RUN useradd -d /home/user -m -c "" user
RUN USER=user && \
    GROUP=user

RUN mkdir -p /home/user/building_energy && \
    chown -R user:user /home/user && \
    chmod -R 775 /home/user/

ENV HOME /home/user
WORKDIR /home/user/building_energy
