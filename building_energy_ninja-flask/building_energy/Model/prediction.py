from tensorflow.keras.models import model_from_json
import pickle
import numpy as np
import pandas as pd


def get_weather_file(fname):
    csvdata = open('./static/data/'+fname, 'r')
    colnames = ['year', 'month', 'day', 'hour', 'minute', 'data_source_unct',
                'temp_air', 'temp_dew', 'relative_humidity',
                'atmospheric_pressure', 'etr', 'etrn', 'ghi_infrared', 'ghi',
                'dni', 'dhi', 'global_hor_illum', 'direct_normal_illum',
                'diffuse_horizontal_illum', 'zenith_luminance',
                'wind_direction', 'wind_speed', 'total_sky_cover',
                'opaque_sky_cover', 'visibility', 'ceiling_height',
                'present_weather_observation', 'present_weather_codes',
                'precipitable_water', 'aerosol_optical_depth', 'snow_depth',
                'days_since_last_snowfall', 'albedo',
                'liquid_precipitation_depth', 'liquid_precipitation_quantity']

    columns_to_drop = ['year', 'month', 'day', 'hour', 'minute', 'data_source_unct',
                       'present_weather_observation', 'precipitable_water', 'days_since_last_snowfall',
                       'liquid_precipitation_depth', 'liquid_precipitation_quantity', 'etrn', 'zenith_luminance',
                       'aerosol_optical_depth', 'albedo', 'ceiling_height', 'present_weather_codes', 'visibility']

    # We only have to skip 6 rows instead of 7 because we have already used
    # the realine call above.
    firstline = csvdata.readline()
    data = pd.read_csv(csvdata, skiprows=6, header=0, names=colnames).drop(columns=columns_to_drop)
    return data

def transform(X, standardizer):
    X-= standardizer.mean_
    X/= standardizer.scale_
    return X

def inverse_transform(X, standardizer):
    X*= standardizer.scale_
    X+= standardizer.mean_
    return X




class server_loaded_model():
    def __init__(self, fname_json, fname_h5, fname_standardizer):
        # load json and create model
        json_file = open(fname_json, 'r')#"cv_l2_1.json", 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)
        # load weights into new model
        self.model.load_weights(fname_h5)#"cv_l2_1.h5")
        print("Loaded model from disk")
        self.model.compile()

        self.standardizer = pickle.load(open(fname_standardizer, 'rb'))#'standarizer.p', 'rb'))


    def perform_prediction(self, fname, building_params):
        weather = get_weather_file(fname)
        weather = transform(weather, self.standardizer[0]).values.ravel()
        print(np.mean(weather))
        params = transform(building_params.reshape(1,-1), self.standardizer[1]).ravel()
        X = np.hstack((weather, params)).reshape(1,-1)
        return np.exp(inverse_transform(self.model.predict(X), self.standardizer[2]))*2.7778e-10

    def perform_prediction_into_csv(self, fname, building_params):
        weather = get_weather_file(fname)
        weather = transform(weather, self.standardizer[0]).values.transpose().ravel()
        print(np.mean(weather))
        params = transform(building_params.reshape(1,-1), self.standardizer[1]).ravel()
        X = np.hstack((weather, params)).reshape(1,-1)
        outputs = np.expm1(inverse_transform(self.model.predict(X), self.standardizer[2]))*2.7778e-10
        #outputs = self.model.predict(X)
        pd.DataFrame(outputs.reshape(-1,1),
                     index=pd.date_range(start='1/1/2020', periods=8760, freq='H')).to_csv('test.csv')








