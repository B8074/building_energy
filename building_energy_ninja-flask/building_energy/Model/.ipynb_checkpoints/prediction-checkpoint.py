from tensorflow.keras.models import model_from_json
import pickle
import numpy as np


# +
def transform(X, standardizer):
    X-= standardizer.mean_
    X/= standardizer.scale_
    return X

def inverse_transform(X, standardizer):
    X*= standardizer.scale_
    X+= standardizer.mean_
    return X


# -

class server_loaded_model():
    def __init__(self, fname_json, fname_h5, fname_standardizer):
        # load json and create model
        json_file = open(fname_json, 'r')#"cv_l2_1.json", 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        self.model = model_from_json(loaded_model_json)
        # load weights into new model
        self.model.load_weights(fname_h5)#"cv_l2_1.h5")
        print("Loaded model from disk")
        self.model.compile()

        self.standardizer = pickle.load(open(fname_standardizer, 'rb'))#'standarizer.p', 'rb'))


    def perform_prediction(self, weather, building_params):
        weather = np.zeros((17*8760,))#self.standardizer[0].transform(weather)
        params = transform(building_params.reshape(1,-1), self.standardizer[1]).ravel()
        X = np.hstack((weather, params)).reshape(1,-1)
        print(self.model.predict(X))
        return inverse_transform(self.model.predict(X), self.standardizer[2])









